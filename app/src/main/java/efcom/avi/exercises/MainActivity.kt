package efcom.avi.exercises

import android.Manifest
import android.annotation.TargetApi
import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.text.Editable
import android.text.Html
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.LineDataSet
import efcom.avi.exercises.chartutils.*
import efcom.avi.exercises.exercises.BLEService
import efcom.avi.exercises.exercises.BLEService.LocalBinder
import efcom.avi.exercises.exercises.ExerciseManager
import efcom.avi.exercises.exercises.algorithm.SetCalculationAlgorithm
import efcom.avi.exercises.exercises.interfaces.IBTCallback
import efcom.avi.exercises.exercises.interfaces.IExerciseManagerListener
import efcom.avi.exercises.exercises.models.HFDevice
import efcom.avi.exercises.exercises.models.Weight
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), IOnDataSetDataChanged, IOnDataSetVisibilityChange {

    /*view*/
    private var mChart: LineChart? = null
    private var mChartCreator: LineChartCreator? = null
    private var mSetsListViewAdapter: DataSetsListAdapter? = null
    private var mSetsListView: ListView? = null
    private var tvCalories: TextView? = null
    private var tvMaxWeightLC1: TextView? = null
    private var tvMaxWeightLC2: TextView? = null
    private var tvRepetitionsLC1: TextView? = null
    private var tvRepetitionsLC2: TextView? = null
    private var tvSetsNumberLC1: TextView? = null
    private var tvSetsNumberLC2: TextView? = null
    private var btnReset: Button? = null
    private var btnSave: Button? = null
    private var btnOpenFolder: Button? = null
    private var btnShare: Button? = null
    private var btnSelectDevices: Button? = null
    private var listView: ListView? = null
    private val mapMaxWeight: MutableMap<String?, TextView?> = HashMap()
    private val mapSetCount: MutableMap<String?, TextView?> = HashMap()
    private val mapRepetition: MutableMap<String?, TextView?> = HashMap()


    private var mBLEService: BLEService? = null
    private val exerciseManager: ExerciseManager? = ExerciseManager.getInstance()
    private val devices: MutableList<HFDevice?> = ArrayList()
    private val flightPlans: ArrayList<String?>? = ArrayList()
    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName?, service: IBinder?) {
            Log.v("hyfit", "onServiceConnected: connect to service")
            mBLEService = (service as LocalBinder?)?.getService()
            mBLEService?.Scanning()

            setListenerForBT()
        }

        override fun onServiceDisconnected(className: ComponentName?) {
            mBLEService = null
        }
    }
    private var getPermission = false
    private var etWeight: EditText? = null
    private var mFile: String? = null
    fun doBindService() {
        Log.v("hyfit", ": bind")
        bindService(Intent(this,
                BLEService::class.java), mConnection, Context.BIND_AUTO_CREATE)
    }

    fun doUnbindBLEService() {
        try {
            if (mBLEService != null) { // Detach our existing connection.
                mBLEService?.onDestroy()
                unbindService(mConnection)
            }
        } catch (e: Exception) {
            Log.v("hyfit", "doUnbindWahooService: " + e.message)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUI()

        askForPermissions()
        checkBluetoothEnable(true) {

        }
        btnReset?.setOnClickListener(View.OnClickListener {
            for (algo in exerciseManager?.getAlgos()!!) {
                algo.reset()
            }
        })

        initListeners()
        initChart()
    }


    private fun setListenerForBT() {

        Log.v("hyfit", "hyfit: setListenerForBT")

        mBLEService?.setListener(object : IBTCallback {
            override fun onBTDeviceReady(bluetoothDevices: MutableList<BluetoothDevice?>?) {

            }

            override fun onGetBTScan(bluetoothDevice: BluetoothDevice?) {
                connect(bluetoothDevice)
            }

            override fun onError(message: String?) {
                Log.v("hyfit", "hyfit: onError ${message}")
            }

            override fun onMessage(message: String?) {

            }
        })
    }

    private var isTryingConnect = false
    private fun connect(bluetoothDevice: BluetoothDevice?) {

        if (devices.size >= 2) {
            if (mBLEService?.isScanning() == true) mBLEService?.stopScan()
            return
        }

        if (isTryingConnect) return

        isTryingConnect = true
        Log.v("hyfit", "hyfit: connect TO ${bluetoothDevice?.address} ${bluetoothDevice?.name}")

        //delay 5sec, waiting for connection
        Handler().postDelayed({

            devices.add(HFDevice(mBLEService?.getBtDeviceByName(bluetoothDevice?.name), baseContext))
            exerciseManager?.listener = (object : IExerciseManagerListener {
                override fun onCalcMaxWeight(name: String?, maxWeight: Int) {
                    val textView = mapMaxWeight[name]
                    textView?.setText(maxWeight.toString())
                }

                override fun onSetStart(deviceName: String?, setsCount: Int) {
                    val textView = mapSetCount[deviceName]
                    textView?.setText(setsCount.toString())
                }

                override fun onRepCount(name: String?, repCount: Int) {
                    val textView = mapRepetition[name]
                    textView?.setText(repCount.toString())
                }

                override fun onCalcCalories(calculateCalories: Double) {
                    tvCalories?.setText(calculateCalories.toInt().toString())
                }

                override fun onDataReceived(name: String?, weight: Weight?) {
                    addData(RssiEntry(weight!!.timeStamp, name, weight.weight))
                }

                override fun removeDevice(hfDevice: HFDevice?) {
                    devices.remove(hfDevice)
                }
            })
            if (devices.size == 1) {
                mapMaxWeight[devices.get(0)?.getName()] = tvMaxWeightLC1
                mapSetCount[devices.get(0)?.getName()] = tvSetsNumberLC1
                mapRepetition[devices.get(0)?.getName()] = tvRepetitionsLC1
                exerciseManager?.initDevices(devices.get(0)!!, null)
            } else {
                mapMaxWeight[devices.get(0)?.getName()] = tvMaxWeightLC1
                mapSetCount[devices.get(0)?.getName()] = tvSetsNumberLC1
                mapRepetition[devices.get(0)?.getName()] = tvRepetitionsLC1
                mapMaxWeight[devices.get(1)?.getName()] = tvMaxWeightLC2
                mapSetCount[devices.get(1)?.getName()] = tvSetsNumberLC2
                mapRepetition[devices.get(1)?.getName()] = tvRepetitionsLC2
                exerciseManager?.initDevices(devices.get(0)!!, devices.get(1))
            }

            isTryingConnect = false
        }, 5000L)
    }

    private fun makeRawDataFile(name: String?, json: String?) {
        var output: Writer? = null
        val mLastStoreFileLog = SimpleDateFormat("yyyyMMdd'T'HHmmss", Locale.US).format(Date())
        var file = File(Environment.getExternalStorageDirectory().path + "/HyfitTest", name + "_" + mLastStoreFileLog + ".csv")
        try {
            Log.v("file", "makeRawDataFile: " + file.parentFile)
            file.parentFile.mkdir()
            if (!file.createNewFile()) {
                file = File(getExternalFilesDir(null).toString() + "/HyfitTest", name + "_" + mLastStoreFileLog + ".csv")
                file.parentFile.mkdir()
            }
        } catch (e: IOException) {
            e.printStackTrace()
            file = File(getExternalFilesDir(null).toString() + "/HyfitTest", name + "_" + mLastStoreFileLog + ".csv")
            file.parentFile.mkdir()
        }
        try {
            output = BufferedWriter(FileWriter(file))
            output.write(json)
            output.close()
            Log.v("Json", "getResultsFromList: Json save file: " + file.absolutePath)
            mFile = file.absolutePath
            runOnUiThread { btnShare?.setEnabled(true) }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun initListeners() {

        btnOpenFolder?.setOnClickListener(View.OnClickListener {
            val selectedUri = Uri.parse(File(mFile).parent)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(selectedUri, "resource/folder")
            startActivity(intent)
        })
        btnSave?.setOnClickListener(View.OnClickListener {
            for (algo in ExerciseManager.Companion.getInstance()?.getAlgos()!!) {
                val moreInfoDialoglayout = LayoutInflater.from(applicationContext).inflate(R.layout.file_dialog, null)
                val deviceName = moreInfoDialoglayout.findViewById<TextView?>(R.id.device_name)
                val moreInfo = moreInfoDialoglayout.findViewById<EditText?>(R.id.more_info)
                val repetition = moreInfoDialoglayout.findViewById<EditText?>(R.id.repetition)
                deviceName?.setText(algo?.getName())
                val alertDialog = AlertDialog.Builder(this@MainActivity, R.style.Theme_AppCompat_DayNight)
                        .setView(moreInfoDialoglayout)
                        .setPositiveButton("save") { dialogInterface, i ->
                            val weightList = algo.getWeightList()
                            val dest = ArrayList<Weight?>(weightList!!)
                            //                    FullWeightAndRep fullWeightAndRep = new FullWeightAndRep(dest, algo.getRepetitionCount());
                            var fileContent = "repetition," + repetition?.getText().toString() + "\n"
                            fileContent += "weight H.T," + SetCalculationAlgorithm.Companion.weightTH + "\n"
                            fileContent += moreInfo?.getText().toString() + "\n"
                            fileContent += "count,timestamp,weight" + "\n"
                            for (i1 in dest.indices) {
                                val weight = dest[i1]!!
                                fileContent += i1.toString() + "," + weight.timeStamp + "," + weight.weight + "\n"
                            }
                            makeRawDataFile(algo?.getName(), fileContent)
                            //                                    btnOpenFolder.setEnabled(true);
                            algo?.reset()
                            algo?.listener?.onRepCount(algo.getName(), 0)
                        }
                        .create()
                val window = alertDialog.window
                window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                alertDialog.show()
            }
        })
        btnShare?.setOnClickListener(View.OnClickListener {
            if (mFile == null) {
                return@OnClickListener
            }
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            val contentUri = FileProvider.getUriForFile(
                    this@MainActivity,
                    "efcom.avi.exercises.provider",
                    File(mFile))
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
            //                    shareIntent.setData(contentUri);
//                    shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.type = "text/plain"
            startActivity(Intent.createChooser(shareIntent, null))
            btnShare?.setEnabled(false)
        })
        etWeight?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence?, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence?, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable?) {
                var weightTH = 0
                if (editable.toString() != "") {
                    weightTH = editable.toString().toInt()
                }
                SetCalculationAlgorithm.Companion.weightTH = weightTH
            }
        })
        val filtertxt = InputFilter { source, start, end, dest, dstart, dend ->
            for (i in start until end) {
                if (Character.isLetter(source[i])) {
                    return@InputFilter ""
                }
            }
            null
        }
        etWeight?.setFilters(arrayOf<InputFilter?>(filtertxt))
    }

    private fun initChart() {
        mChart = findViewById<View?>(R.id.chart) as LineChart?
        mChartCreator = LineChartCreator(applicationContext, mChart, this)
        mSetsListView = findViewById<View?>(R.id.DataSetsListView) as ListView?
        //        mSetsListViewAdapter = new DataSetsListAdapter(getApplicationContext());
        mSetsListViewAdapter = DataSetsListAdapter(this)
        mSetsListViewAdapter?.setIOnDataSetVisibilityChange(this)
        mSetsListView?.setAdapter(mSetsListViewAdapter)
        mSetsListViewAdapter?.updateList(mChartCreator?.getAllDataSets())
    }

    override fun onDestroy() {
        super.onDestroy()
        doUnbindBLEService()
    }

    override fun onChange(updatedSets: ArrayList<LineDataSet?>?) {
        try { //            runOnUiThread(()->{
            mSetsListViewAdapter?.updateList(updatedSets)
            //            });
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun makeDataSetVisible(lineDataSet: LineDataSet?) {
        mChartCreator?.makeDataSetVisible(lineDataSet)
    }

    override fun makeDataSetInvisible(lineDataSet: LineDataSet?) {
        mChartCreator?.makeDataSetInvisible(lineDataSet)
    }

    override fun isDataSetCurrentlyVisible(lineDataSet: LineDataSet?): Boolean {
        return mChartCreator!!.isDataSetCurrentlyVisible(lineDataSet)
    }

    fun addData(rssiEntry: RssiEntry?) {
//        Log.v("TAG", "addData - " + rssiEntry.getMACAddress() + " " + rssiEntry.getRssiVal())
        runOnUiThread {
            try {
                Log.v("TAG", "run: addData")
                mChartCreator?.addEntry(rssiEntry)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun openDevicesList() {
        flightPlans?.clear()
        mBLEService?.getDevicesNames()?.let { flightPlans?.addAll(it) }
        val d = Dialog(this@MainActivity)
        d.setContentView(R.layout.dialog_devices_ble)
        val lv = d.findViewById<ListView?>(R.id.lv_flightPlans)
        lv?.setAdapter(ArrayAdapter(d.context, android.R.layout.simple_list_item_1, flightPlans!!))
        lv?.setOnItemClickListener(OnItemClickListener { parent, view, position, id ->
            d.dismiss()
            devices.add(HFDevice(mBLEService?.getBtDeviceByName(flightPlans?.get(position)), baseContext))
            exerciseManager?.listener = (object : IExerciseManagerListener {
                override fun onCalcMaxWeight(name: String?, maxWeight: Int) {
                    val textView = mapMaxWeight[name]
                    textView?.setText(maxWeight.toString())
                }

                override fun onSetStart(deviceName: String?, setsCount: Int) {
                    val textView = mapSetCount[deviceName]
                    textView?.setText(setsCount.toString())
                }

                override fun onRepCount(name: String?, repCount: Int) {
                    val textView = mapRepetition[name]
                    textView?.setText(repCount.toString())
                }

                override fun onCalcCalories(calculateCalories: Double) {
                    tvCalories?.setText(calculateCalories.toInt().toString())
                }

                override fun onDataReceived(name: String?, weight: Weight?) {
                    addData(RssiEntry(weight!!.timeStamp, name, weight.weight))
                }

                override fun removeDevice(hfDevice: HFDevice?) {
                    devices.remove(hfDevice)
                }
            })
            if (devices.size == 1) {
                mapMaxWeight[devices.get(0)?.getName()] = tvMaxWeightLC1
                mapSetCount[devices.get(0)?.getName()] = tvSetsNumberLC1
                mapRepetition[devices.get(0)?.getName()] = tvRepetitionsLC1
                exerciseManager?.initDevices(devices.get(0)!!, null)
            } else {
                mapMaxWeight[devices.get(0)?.getName()] = tvMaxWeightLC1
                mapSetCount[devices.get(0)?.getName()] = tvSetsNumberLC1
                mapRepetition[devices.get(0)?.getName()] = tvRepetitionsLC1
                mapMaxWeight[devices.get(1)?.getName()] = tvMaxWeightLC2
                mapSetCount[devices.get(1)?.getName()] = tvSetsNumberLC2
                mapRepetition[devices.get(1)?.getName()] = tvRepetitionsLC2
                exerciseManager?.initDevices(devices.get(0)!!, devices.get(1))
            }
        })
        if (flightPlans?.size!! > 0) d.show() else AlertDialog.Builder(this@MainActivity).setPositiveButton("OK") { dialog, which -> dialog.dismiss() }.setCancelable(false).setTitle("Error!").setMessage("No found any Hyfit device.").show()
    }

    private fun showMessageOKOnly(message: String?, okListener: DialogInterface.OnClickListener?) {
        Log.v("TAG", "showMessageOKAndCancel: message: $message")
        AlertDialog.Builder(this)
                .setMessage(Html.fromHtml(message))
                .setCancelable(false)
                .setPositiveButton("ok", okListener)
                .create()
                .show()
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun askForPermissions() {
        val permissionsNeeded: MutableList<String?> = ArrayList()
        val permissionsList: MutableList<String?> = ArrayList()
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION)) permissionsNeeded.add("Show Location")
        /**
         * un commit the if below you use [RemoteCloudInteractor.makeJsonFile]
         */
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE)) permissionsNeeded.add("WRITE EXTERNAL STORAGE")
        if (permissionsList.size > 0) {
            if (permissionsNeeded.size > 0) { // Need Rationale
                val message = StringBuilder("App need access to " + permissionsNeeded[0])
                for (i in 1 until permissionsNeeded.size) message.append(", ").append(permissionsNeeded[i])
                showMessageOKOnly(message.toString(),
                        DialogInterface.OnClickListener { dialog, which ->
                            requestPermissions(permissionsList.toTypedArray(),
                                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
                        })
                return
            }
            requestPermissions(permissionsList.toTypedArray(),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            return
        }
        getPermission = true
        //can start advertise
        allPermissionGranted()
    }

    private fun allPermissionGranted() {
        val i = Intent(this, BLEService::class.java)
        startService(i)
        doBindService()
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun addPermission(permissionsList: MutableList<String?>?, permission: String?): Boolean {
        if (checkSelfPermission(permission.toString()) != PackageManager.PERMISSION_GRANTED) {
            permissionsList?.add(permission)
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission.toString())) return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                val perms: MutableMap<String?, Int?> = HashMap()
                // Initial
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                // Fill with results
                var i = 0
                while (i < permissions.size) {
                    perms[permissions.get(i)] = grantResults.get(i)
                    i++
                }
                // Check for ACCESS_FINE_LOCATION
                if (perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED) { // All Permissions Granted
                    Toast.makeText(this, "permission granted ", Toast.LENGTH_SHORT)
                            .show()
                    getPermission = true
                    //can start advertise
                    allPermissionGranted()
                } else { // Permission Denied
                    Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT)
                            .show()
                    finish()
                }
            }
            else -> grantResults?.let { super.onRequestPermissionsResult(requestCode, permissions!!, it) }
        }
    }

    private fun initUI() {
        etWeight = findViewById<EditText?>(R.id.weightTH)
        listView = findViewById<ListView?>(R.id.list_view)
        btnReset = findViewById<Button?>(R.id.reset)
        btnSave = findViewById<Button?>(R.id.save)
        btnOpenFolder = findViewById<Button?>(R.id.open_folder)
        btnShare = findViewById<Button?>(R.id.share)
        btnSelectDevices = findViewById<Button?>(R.id.select_devices)
        tvCalories = findViewById<TextView?>(R.id.calories)
        tvMaxWeightLC1 = findViewById<TextView?>(R.id.max_weight_lc1)
        tvMaxWeightLC2 = findViewById<TextView?>(R.id.max_weight_lc2)
        tvRepetitionsLC1 = findViewById<TextView?>(R.id.repetitions_lc1)
        tvRepetitionsLC2 = findViewById<TextView?>(R.id.repetitions_lc2)
        tvSetsNumberLC1 = findViewById<TextView?>(R.id.sets_number_lc1)
        tvSetsNumberLC2 = findViewById<TextView?>(R.id.sets_number_lc2)
    }


    private val mPairReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context1: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED == action) {
                val state =
                        intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR)
                val prevState = intent.getIntExtra(
                        BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE,
                        BluetoothDevice.ERROR
                )
                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {

                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {


                }
            }
        }
    }

    fun checkBluetoothEnable(isNeededRequest: Boolean, action: (isBtEnable: Boolean) -> Unit) {

        val bluetoothManager =
                getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val mBluetoothAdapter = bluetoothManager.adapter

        val intent = IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        registerReceiver(mPairReceiver, intent)

        if (mBluetoothAdapter == null || mBluetoothAdapter?.isEnabled != true) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            if (isNeededRequest)
                startActivityForResult(enableBtIntent, 10001)
            action(false)
            return
        } else {
            action(true)
        }
    }

    companion object {
        private const val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 123
    }


}