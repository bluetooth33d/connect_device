package efcom.avi.exercises.exercises.interfaces

import efcom.avi.exercises.exercises.models.HFDevice
import efcom.avi.exercises.exercises.models.Weight

interface IDeviceListener {
    open fun onDataReceived(hfDevice: HFDevice?, weights: MutableList<Weight>)
    open fun onDeviceStateChange(hfDevice: HFDevice?, state: Int)
    open fun onBatteryLevelReceived(hfDevice: HFDevice?, batteryLevel: Int)
    open fun onVersionReceived(hfDevice: HFDevice?, version: String?)
    open fun onCalibrationDone(hfDevice: HFDevice?)
}