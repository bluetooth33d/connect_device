package efcom.avi.exercises.exercises.algorithm

import android.util.Log
import efcom.avi.exercises.exercises.interfaces.ISetCalculationAlgorithmListener
import efcom.avi.exercises.exercises.models.SetInfo
import efcom.avi.exercises.exercises.models.Weight
import java.util.*

class SetCalculationAlgorithm(private val name: String?) {
    private val weights: MutableList<Weight?>?
    private val algos: MutableList<Algo?> = ArrayList()
    var listener: ISetCalculationAlgorithmListener? = null
    private var isPause = false
    private var algoIndex = 0
    private var maxPower = 0.0
    var repTrackers: ArrayList<RepTracker?>?

    fun getName(): String? {
        return name
    }

    fun addData(newWeights: MutableList<Weight>) {
        var reps = repTrackers?.get(repTrackers!!.size - 1)?.reps
        val velocity = DoubleArray(newWeights!!.size - 1)
        val power = DoubleArray(newWeights.size - 1)
        for (i in 0 until newWeights.size - 1) {
            velocity[i] = (newWeights.get(i + 1)!!.weight - newWeights.get(i)!!.weight).toDouble() / (newWeights.get(i + 1)!!.timeStamp - newWeights.get(i)!!.timeStamp).toDouble()
            power[i] = Math.abs(velocity[i]) * newWeights.get(i)!!.weight / 1000
        }
        if (!isPause) {
            weights?.addAll(newWeights)
            for (v in power) {
                if (maxPower < v) {
                    maxPower = v
                }
            }
            reps = algos.get(algoIndex)?.AppendPoints(newWeights)
        }
//        Log.v(TAG, "addData1: " + (Date().time - repTrackers.get(repTrackers.size - 1).getTime()).toString() + ": " + repTrackers.get(repTrackers.size - 1).getReps() + ": " + repTrackers.get(repTrackers.size - 1).getReps() + ":" + reps)
        if (Date().time - repTrackers?.get(repTrackers!!.size - 1)!!.time > 15 * 1000 && repTrackers!!.get(repTrackers!!.size - 1)?.reps != 0 && repTrackers!!.get(repTrackers!!.size - 1)?.reps == reps) { //set end
            listener?.onRepCount(name, reps!!)
            reset()
            repTrackers?.add(RepTracker(Date().time, 0))
            endSet()
        }
        Log.v(TAG, "addData1: avg vel:" + avg(velocity))
        if (avg(velocity) > 1 || repTrackers!!.get(repTrackers!!.size - 1)?.reps != reps) repTrackers!!.add(RepTracker(Date().time, reps!!))
        listener?.onRepCount(name, reps!!)
    }

    private fun avg(velocity: DoubleArray?): Double {
        var vel = 0.0
        for (v in velocity!!) {
            vel += v
        }
        return vel / velocity.size
    }

    fun startSet() {
        if (!isPause) {
            reset()
            listener?.onSetStart(name)
        } else {
            isPause = false
        }
    }

    fun endSet() { // TODO: 24/05/2020 collect the data and make it #SetInfo object
//mock end
        val setInfo = SetInfo(getSetActiveDuration(), getSetRestDuration(), getSetMaxPower(), getSetMaxWeight(), getTotalRepetitionCount(), getSetWork())
        listener?.onSetEnd(setInfo, "end")
    }

    private fun getSetWork(): Double {
        var maxWork = 0.0
        for (algo1 in algos) {
            for (repInfo in algo1?.minMaxMinList!!) {
                val work = repInfo?.FWHH!! * repInfo.maxW * 100
                if (maxWork < work) maxWork = work
            }
        }
        return maxWork
    }

    private fun getSetMaxWeight(): Int {
        var max = 0
        for (algo1 in algos!!) {
            if (max < algo1!!.getMaxWeight()) max = algo1.getMaxWeight().toInt()
        }
        return max
    }

    private fun getSetMaxPower(): Double {
        return maxPower
    }

    fun pauseSet() {
        isPause = true
    }

    fun resumeSet() {
        isPause = false
    }

    fun getMaxWeight(): Int {
        return algos?.get(algoIndex)?.getMaxWeight()?.toInt()!!;
    }

    fun getTotalRepetitionCount(): Int {
        var repetition = 0
        for (algo1 in algos!!) {
            repetition += algo1!!.reps
        }
        return repetition
    }

    fun getWeightList(): MutableList<Weight?>? {
        return weights
    }

    fun getSetActiveDuration(): Int {
        var duration: Long = 0
        var timeDelta = repTrackers!!.get(repTrackers!!.size - 1)!!.time
        var state = 0
        Log.v(TAG, "getSetActiveDuration: start")
        for (i in repTrackers!!.indices.reversed()) {
            val repTracker = repTrackers?.get(i)
            if (repTracker?.reps != 0) {
                state = 1
                Log.v(TAG, "getSetActiveDuration: start seek 0")
            }
            if (repTracker?.reps == 0 && state == 0) {
                Log.v(TAG, "getSetActiveDuration: set delta " + repTracker.time)
                timeDelta = repTracker.time
            }
            if (repTracker?.reps == 0 && state == 1) {
                Log.v(TAG, "getSetActiveDuration: found 0 calc")
                state = 0
                duration += timeDelta - repTracker.time
            }
        }
        Log.v(TAG, "getSetActiveDuration: end")
        return duration.toInt() / 1000
    }

    fun getSetRestDuration(): Int {
        val duration = Date().time - repTrackers?.get(0)?.time!!
        val timeDelta = duration / 1000
        return timeDelta.toInt() - getSetActiveDuration()
    }

    fun reset() { //        weights.clear();
        val algo = Algo()
        algos.add(algo)
        algoIndex++
        listener?.onRepCount(name, 0)
    }


    inner class RepTracker(var time: Long, var reps: Int) {


    }

    companion object {
        private val TAG: String? = SetCalculationAlgorithm::class.java.simpleName
        var weightTH = 500
    }

    init {
        weights = ArrayList()
        val algo = Algo()
        repTrackers = ArrayList()
        repTrackers?.add(RepTracker(Date().time, 0))
        algos.add(algo)
    }
}