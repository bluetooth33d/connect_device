package efcom.avi.exercises.exercises

import java.text.SimpleDateFormat
import java.util.*

object Constants {
    val LS_SERVICE = UUID.fromString("fc690001-aaf5-432f-8a8b-8feb7dd9c4ab")
    val FIRST_CHAR = UUID.fromString("fc690003-aaf5-432f-8a8b-8feb7dd9c4ab")
    val SECEOND_CHAR = UUID.fromString("fc690002-aaf5-432f-8a8b-8feb7dd9c4ab")
    val DESCRIPTOR_LS = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
    val BATTERY_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb")
    val BATTERY_LEVEL = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb")
    var sFromJsonToStringCapsuleInfo: SimpleDateFormat? = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH) //    public static String getDeviceUpdateStatus(Context c) {
//        SharedPreferences sharedPref = c.getSharedPreferences("ABA", MODE_PRIVATE);
//        return sharedPref.getString("DeviceUpdateStatus", new DeviceUpdateStatus().toString());
//    }
//
//    public static void setDeviceUpdateStatus(Context context, String tt) {
//        Log.v("PAP", "DeviceUpdateStatus => " + tt);
//        SharedPreferences sharedPref = context.getSharedPreferences("ABA", MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString("DeviceUpdateStatus", tt).apply();
//    }
}