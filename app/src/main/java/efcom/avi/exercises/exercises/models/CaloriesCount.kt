package efcom.avi.exercises.exercises.models

object CaloriesCount {
    fun CalculateCalories(duration: Int, restDuration: Int, userAge: Int, userWeight: Float, userGender: Int): Double {
        return userGender * ((-55.0969 + 0.6309 * 0.95 * HRmax(userGender, userAge) + 0.1988 * userWeight + 0.2017 * userAge) * duration + (-55.0969 + 0.6309 * 0.75 * HRmax(userGender, userAge) + 0.1988 * userWeight + 0.2017 * userAge) * restDuration) +
                (1 - userGender) * ((-20.4022 + 0.4472 * 0.95 * HRmax(userGender, userAge) - 0.1263 * userWeight + 0.074 * userAge) * duration + (-20.4022 + 0.4472 * 0.75 * HRmax(userGender, userAge) - 0.1263 * userWeight + 0.074 * userAge) * restDuration)
    }

    fun HRmax(gender: Int, age: Int): Double {
        return gender * (208.609 - 0.716 * age) + (1 - gender) * (209.273 - 0.804 * age)
    }
}