package efcom.avi.exercises.exercises.interfaces

import android.bluetooth.BluetoothDevice

interface IBTCallback {
    open fun onBTDeviceReady(bluetoothDevices: MutableList<BluetoothDevice?>?)
    open fun onGetBTScan(bluetoothDevice: BluetoothDevice?)
    open fun onError(message: String?)
    open fun onMessage(message: String?)
}