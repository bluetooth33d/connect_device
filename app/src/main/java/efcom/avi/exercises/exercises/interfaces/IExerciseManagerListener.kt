package efcom.avi.exercises.exercises.interfaces

import efcom.avi.exercises.exercises.models.HFDevice
import efcom.avi.exercises.exercises.models.Weight

interface IExerciseManagerListener {
    open fun onCalcMaxWeight(name: String?, maxWeight: Int)
    open fun onSetStart(deviceName: String?, setsCount: Int)
    open fun onRepCount(name: String?, repCount: Int)
    open fun onCalcCalories(calculateCalories: Double)
    open fun onDataReceived(name: String?, weight: Weight?)
    open fun removeDevice(hfDevice: HFDevice?)
}