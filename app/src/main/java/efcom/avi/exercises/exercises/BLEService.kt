package efcom.avi.exercises.exercises

import android.app.Service
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.ParcelUuid
import android.util.Log
import efcom.avi.exercises.exercises.BLEService
import efcom.avi.exercises.exercises.interfaces.IBTCallback
import java.nio.charset.StandardCharsets
import java.util.*

class BLEService : Service() {
    private val mDeviceList: HashMap<String?, BluetoothDevice?>? = HashMap()
    private val mDeviceListMonitor: HashMap<String?, Long?>? = HashMap()
    private var bluetoothAdapter: BluetoothAdapter? = null
    var btManager: BluetoothManager? = null
    private var isScan = false
    private val mBinder: IBinder? = LocalBinder()
    private var context: Context? = null
    private var mStopHandler: Handler? = null
    private var attemptConnect = 0
    private var scanCallback: ScanCallback? = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            //Log.v(TAG, "onLeScan: Mac=> " + result?.getDevice()?.address + " Rssi=> " + result?.getRssi() + " name=> " + result?.getDevice()?.name)
            if (result?.getDevice()?.name != null) processDeviceDiscovered(result.getDevice(), result.getRssi(), result.getScanRecord()?.getBytes())
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Log.v(TAG, "onScanFailed: $errorCode")
            listener?.onError("onScanFailed")
        }
    }
    private val mExtraInfo: ByteArray? = ByteArray(0)
    private var listener: IBTCallback? = null
    /*
       public void sendCommend(String commend, byte[] bytes) {
    //        first = true;

            if (mBluetoothGatt == null) {
                if (!isScan) {
                    setScan(true);
                    if (bluetoothAdapter.isEnabled())
                        bluetoothAdapter.startLeScan(leScanCallback);
                }
    //            if(mWahooApiCallback!=null)
    //                mWahooApiCallback.onError("Error occur please reset the app");
                return;
            }
    //        mState = Integer.parseInt(commend);
            mExtraInfo = bytes;
            startNextCommend(mBluetoothGatt);
        }
        */
    private fun startNextCommend(gatt: BluetoothGatt) {
        var characteristic: BluetoothGattCharacteristic? = null
        if (gatt.getService(Constants.LS_SERVICE) != null || gatt.getService(Constants.LS_SERVICE).getCharacteristic(Constants.FIRST_CHAR) != null) characteristic = gatt.getService(Constants.LS_SERVICE).getCharacteristic(Constants.FIRST_CHAR)
        var count: Byte = 0
        var i = 0
        while (i < mExtraInfo?.size!!) {
            // TODO: 13/04/2020 add flag
            val range = Arrays.copyOfRange(mExtraInfo, i, Math.min(i + 17, mExtraInfo.size))
            val commend = ByteArray(range.size + 3)
            System.arraycopy(range, 0, commend, 3, range.size)
            commend[0] = 0x0a
            commend[1] = count++
            characteristic?.setValue(commend)
            Log.v(TAG, "startNextCommend: command: " + Arrays.toString(commend))
            try {
                Thread.sleep(100)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            gatt.writeCharacteristic(characteristic)
            i += 17
        }
    }

    fun calibrateLC(gatt: BluetoothGatt) {
        var characteristic: BluetoothGattCharacteristic? = null
        characteristic = if (gatt.getService(Constants.LS_SERVICE) != null && gatt.getService(Constants.LS_SERVICE).getCharacteristic(Constants.SECEOND_CHAR) != null) gatt.getService(Constants.LS_SERVICE).getCharacteristic(Constants.SECEOND_CHAR) else {
            listener?.onError("service or characteristic not found")
            return
        }
        characteristic.setValue("AT+CALI=LC\r\n")
        val sb = StringBuilder()
        for (b in characteristic.value) {
            sb.append(String.format("%02X ", b))
        }
        Log.v(TAG, "calibrateLC: data:$sb")
        gatt.writeCharacteristic(characteristic)
    }

    fun getVersion() {
        val gatts = ArrayList(mBluetoothGattHashMap?.values!!)
        if (gatts.size == 0) {
            listener?.onError("connect the device")
            return
        }
        var characteristic: BluetoothGattCharacteristic? = null
        for (gatt in gatts) {
            characteristic = if (gatt!!.getService(Constants.LS_SERVICE) != null && gatt.getService(Constants.LS_SERVICE).getCharacteristic(Constants.SECEOND_CHAR) != null) gatt.getService(Constants.LS_SERVICE).getCharacteristic(Constants.SECEOND_CHAR) else {
                listener?.onError("service or characteristic not found")
                break
            }
            characteristic.setValue("AT+VERSION\r\n")
            val sb = StringBuilder()
            for (b in characteristic.value) {
                sb.append(String.format("%02X ", b))
            }
            Log.v(TAG, "calibrateLC: data:$sb")
            gatt.writeCharacteristic(characteristic)
        }
    }

    /*
    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.v(TAG, "onConnectionStateChange: gatt, status, newState:" + gatt + " " + status + " " + newState);
            listener.onGetBTScan(gatt.getDevice());
            boolean repleace = false;
            if (newState == BluetoothGatt.STATE_CONNECTED && status == BluetoothGatt.GATT_SUCCESS) {
                mBluetoothGattHashMap.put(gatt.getDevice().getAddress(), gatt);


                gatt.discoverServices();
            } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                mBluetoothGattHashMap.remove(gatt.getDevice().getAddress());

                for (String s : mBluetoothGattHashMap.keySet()) {
                    if (btManager.getConnectionState(mBluetoothGattHashMap.get(s).getDevice(), GATT) == STATE_DISCONNECTED) {
                        mBluetoothGattHashMap.remove(s);
                    }
                }

            }
            if (status == BluetoothGatt.GATT_INSUFFICIENT_AUTHENTICATION) {
                if (listener != null)
                    listener.onError("GATT_INSUFFICIENT_AUTHENTICATION");
            } else if (status == 133) {
                listener.onError("perform long click on the belt button");
            }
 //else if (gatt.getDevice().getBondState()==BluetoothDevice.BOND_NONE&&status == 22) {
//                listener.onError("Create bond");
//                gatt.getDevice().createBond();
//            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            mBluetoothGattHashMap.put(gatt.getDevice().getAddress(), gatt);

            listener.onGetBTScan(gatt.getDevice());
//            readNotify();
            for (BluetoothGattService service : gatt.getServices()) {
                Log.v(TAG, "onServicesDiscovered: service: " + service.getUuid());
                for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                    Log.v(TAG, "onServicesDiscovered: char: " + characteristic.getUuid());
                }
            }
            enableNotify(gatt);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.v(TAG, "onCharacteristicRead: characteristic:" + characteristic.getUuid() + " " + Arrays.toString(characteristic.getValue()));
            String ref = gatt.getDevice().getName();
            if (ref == null) {
                ref = gatt.getDevice().getAddress();
            }
            if (characteristic.getUuid().equals(BATTERY_LEVEL))
                if (listener != null)
                    listener.onBatteryDataReadReceived(ref, characteristic.getValue());
            if (characteristic.getUuid().equals(BATTERY_LEVEL))
                if (listener != null)
                    listener.onBatteryDataReadReceived(ref, characteristic.getValue());


        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.v(TAG, "onCharacteristicWrite: characteristic:" + characteristic.getUuid() + " " + Arrays.toString(characteristic.getValue()));
            Log.v(TAG, "onCharacteristicWrite: characteristic:" + characteristic.getUuid() + " " + new String(characteristic.getValue()));
            String action = new String(characteristic.getValue()).trim();

            if (listener != null) {
                switch (action) {
                    case "AT+CALI=LC":
                        listener.onMessage("Calibrate LC: " + gatt.getDevice().getName() + "::" + gatt.getDevice().getAddress());

                        break;
                    case "AT+VERSION":
                        listener.onMessage("get version from: " + gatt.getDevice().getName() + "::" + gatt.getDevice().getAddress());

                        break;
                }
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.v(TAG, "onCharacteristicChanged: characteristic:" + characteristic.getUuid() + " " + Arrays.toString(characteristic.getValue()));

            String ref = gatt.getDevice().getName();
            if (ref == null) {
                ref = gatt.getDevice().getAddress();
            }

            if (listener != null) {
                switch (characteristic.getValue()[0]) {
                    case -10:
                        listener.onVersionDataReadReceived(ref, characteristic.getValue());
                        break;
                    case -11:
                        listener.onDataReceived(ref, characteristic.getValue());
                        break;
                }
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
            mBluetoothGattHashMap.put(gatt.getDevice().getAddress(), gatt);

            listener.onGetBTScan(gatt.getDevice());
            enableNotify(gatt);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            mBluetoothGattHashMap.put(gatt.getDevice().getAddress(), gatt);

            attemptConnect = 0;
            listener.onGetBTScan(gatt.getDevice());

        }
    };
**/
    fun getBattery() {
        var characteristic: BluetoothGattCharacteristic
        val gatts = ArrayList(mBluetoothGattHashMap!!.values)
        if (gatts.size == 0) {
            listener?.onError("connect the device")
            return
        }
        for (gatt in gatts) {
            if (gatt!!.getService(Constants.BATTERY_SERVICE) != null) {
                characteristic = gatt.getService(Constants.BATTERY_SERVICE).getCharacteristic(Constants.BATTERY_LEVEL)
                gatt.readCharacteristic(characteristic)
            }
        }
    }

    private fun enableNotify(bluetoothGatt: BluetoothGatt) {
        val characteristic: BluetoothGattCharacteristic?
        characteristic = bluetoothGatt.getService(Constants.LS_SERVICE)?.getCharacteristic(Constants.FIRST_CHAR)
        bluetoothGatt.setCharacteristicNotification(characteristic, true)
        val descriptor = characteristic?.getDescriptor(Constants.DESCRIPTOR_LS)
        descriptor?.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        bluetoothGatt.writeDescriptor(descriptor)
    }

    private fun setScan(scanning: Boolean) {
        if (bluetoothAdapter == null) return
        if (!scanning) {
            if (bluetoothAdapter?.isEnabled()!!) {
                bluetoothAdapter?.getBluetoothLeScanner()?.stopScan(scanCallback)
            }
        } else { // TODO: 20/04/2020 close gatt for show in the scan
//            if (mBluetoothGatt1 != null) {
//                mBluetoothGatt1.close();
//            }
//            if (mBluetoothGatt2 != null) {
//                mBluetoothGatt2.close();
//            }
//
//            if (mBluetoothGatt1 == null && mBluetoothGatt2 == null) {
//                Log.v(TAG, "setScan: Error: mBluetoothGatt is null");
//
//            }
/* else if (btManager.getConnectedDevices(BluetoothGatt.GATT).size() != 0) {
                for (BluetoothDevice bluetoothDevice : btManager.getConnectedDevices(BluetoothGatt.GATT)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        mBluetoothGatt = bluetoothDevice.connectGatt(context, false, bluetoothGattCallback, BluetoothDevice.TRANSPORT_LE);
                    } else {
                        mBluetoothGatt = bluetoothDevice.connectGatt(context, false, bluetoothGattCallback);
                    }
                }
                return;
            }*/
        }
        // TODO: 31/03/2020 set listener to scan changes
        isScan = scanning
    }

    fun isScanning(): Boolean { // TODO: 31/03/2020 set listener to result (isScan)
        return isScan
    }

    fun isDeviceConnect(address: String?): Boolean {
        return if (mBluetoothGattHashMap?.get(address) == null || mBluetoothGattHashMap!![address]?.getDevice() == null) false else btManager?.getConnectionState(mBluetoothGattHashMap!![address]?.getDevice(), BluetoothProfile.GATT) == BluetoothProfile.STATE_CONNECTED
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i("SpicerrBTservice", "Received start id $startId: $intent")
        btManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = btManager?.getAdapter()
        context = applicationContext
        //        Scanning(); //startScan
        return START_STICKY
    }

    fun Scanning() {
        Log.v(TAG, "Scanning: start")
        if (btManager == null) {
            btManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        }
        Log.v(TAG, "Scanning: init adapter")
        if (btManager != null) {
            bluetoothAdapter = btManager?.getAdapter()
        }
        if (bluetoothAdapter == null) {
            Log.v(TAG, "Scanning: bluetoothAdapter == null")
            // TODO: 31/03/2020 handle BT error
            return
        }
        val settings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build()
        val scanFilter = ScanFilter.Builder()
                .build()
        val scanFilters: MutableList<ScanFilter?> = ArrayList()
        val scanSettings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                .setNumOfMatches(ScanSettings.MATCH_NUM_ONE_ADVERTISEMENT)
                .setReportDelay(0L)
                .build()
        val serviceUUIDs = arrayOf(Constants.LS_SERVICE)
        var filters: MutableList<ScanFilter?>? = null
        if (serviceUUIDs != null) {
            filters = ArrayList()
            for (serviceUUID in serviceUUIDs) {
                val filter = ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid(serviceUUID))
                        .build()
                filters.add(filter)
            }
        }
        scanFilters.add(scanFilter)
        if (!bluetoothAdapter?.isEnabled()!!) { // TODO: 31/03/2020 BT to function
            if (isScan) setScan(false)
            //            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            context.startActivity(enableBtIntent);
//            if(mWahooApiCallback!=null)
//            mWahooApiCallback.onScanChange(false);
        }
        if (bluetoothAdapter?.isEnabled()!!) {
            Log.v(TAG, "Scanning: start scan")
            bluetoothAdapter?.getBluetoothLeScanner()?.startScan(filters, scanSettings, scanCallback)
            setScan(true)
        }
        if (mStopHandler != null) mStopHandler?.removeCallbacksAndMessages(null)
        mStopHandler = Handler()
        mStopHandler?.postDelayed(Runnable {
            if (isScan) {
                setScan(false)
                Log.v(TAG, "Scanning: stop scan")
            }
        }, 120 * 1000.toLong())
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isScan) {
            if (bluetoothAdapter != null && bluetoothAdapter?.isEnabled()!!) setScan(false)
        }
        for (value in mBluetoothGattHashMap?.values!!) {
            val address = value?.getDevice()?.address
            value?.disconnect()
            value?.close()
            mBluetoothGattHashMap?.remove(address)
        }
        stopSelf()
    }

    fun stopScan() {
        setScan(false)
    }

    fun getBluetoothAdapter(): BluetoothAdapter? {
        return bluetoothAdapter
    }

    fun setBluetoothAdapter(bluetoothAdapter: BluetoothAdapter?) {
        this.bluetoothAdapter = bluetoothAdapter
    }


    fun getContext(): Context? {
        return context
    }

    fun setContext(context: Context?) {
        this.context = context
    }

    fun getScanCallback(): ScanCallback? {
        return scanCallback
    }

    fun setScanCallback(scanCallback: ScanCallback?) {
        this.scanCallback = scanCallback
    }

    fun reset(bluetoothGatt: BluetoothGatt?) {
        if (bluetoothGatt != null) {
            val address = bluetoothGatt.device.address
            bluetoothGatt.disconnect()
            bluetoothGatt.close()
            mBluetoothGattHashMap?.remove(address)
            //            close();
        } else { //            for (BluetoothDevice bluetoothDevice : mBluetoothGatt.getConnectedDevices()) {
//                bluetoothDevice.
//            }
            Log.v(TAG, "reset: null")
        }
    }

    fun connectToDevice(address: String?) { // TODO: 31/03/2020  notify of connection start process
        val isFirst = true
        if (mDeviceList?.get(address) != null) {
            val device = mDeviceList[address]
            if (device == null) {
               listener?.onError("Device with address " + address + "not found")
                return
            }
            listener?.onGetBTScan(device)
            setScan(false)
            var isDeviceConnect = false
            val bluetoothGatt = mBluetoothGattHashMap?.get(device.address)
            if (bluetoothGatt != null) {
                isDeviceConnect = btManager?.getConnectionState(bluetoothGatt.device, BluetoothProfile.GATT) != BluetoothProfile.STATE_DISCONNECTED
                reset(bluetoothGatt)
            }
            if (!isDeviceConnect) {
                Thread(Runnable {
                    try {
                        Thread.sleep(5 * 1000.toLong())
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                    if (attemptConnect > 5) { // TODO: 31/03/2020 handle connection problem
                        return@Runnable
                    }
                    val bluetoothGatt = mBluetoothGattHashMap?.get(device.address)
                    var checkDeviceConnect = false
                    if (bluetoothGatt != null) checkDeviceConnect = btManager?.getConnectionState(bluetoothGatt.device, BluetoothProfile.GATT) != BluetoothProfile.STATE_DISCONNECTED
                    if (!checkDeviceConnect) {
                        attemptConnect++
                        connectToDevice(address)
                    }
                }).start()
                //                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    Log.v(TAG, "processDeviceDiscovered: connectGatt");
//                    bluetoothGatt = device.connectGatt(context, false, bluetoothGattCallback, BluetoothDevice.TRANSPORT_LE);
//                } else {
//                    bluetoothGatt = device.connectGatt(context, false, bluetoothGattCallback);
//                }
//                mBluetoothGattHashMap.put(device.getAddress(), bluetoothGatt);
            }
        } else { // TODO: 31/03/2020 handle address to connect not found
        }
    }

    private fun processDeviceDiscovered(device: BluetoothDevice?, rssi: Int, scanRecord: ByteArray?) {
        //Log.v(TAG, "processDeviceDiscovered: device name => " + device?.getName() + " device mac => " + device?.getAddress() + " string => " + String(scanRecord!!) + "\nchar => " + Arrays.toString(String(scanRecord, StandardCharsets.UTF_8).toCharArray()))
        val address = device?.getAddress()
        val doUpdate = false
        if (address != null) {
            listener?.onGetBTScan(device)
            if (!mDeviceList?.keys?.contains(address)!!) {
                mDeviceList[address] = device
                mDeviceListMonitor?.set(address, Date().time)
            }
        }
        //        for (String s : mDeviceListMonitor.keySet()) {
//            Long aDeviceLastTime = mDeviceListMonitor.get(s);
//            if (aDeviceLastTime != null)
//                if (new Date().getTime() - aDeviceLastTime > 20 * 1000)
//                    if (!(btManager.getConnectionState(mDeviceList.get(s), GATT) == STATE_CONNECTED)) {
//                        doUpdate = true;
//                        mDeviceList.remove(s);
//                    }
//        }
        if (doUpdate && listener != null) listener?.onBTDeviceReady(ArrayList(mDeviceList?.values!!))
    }

    fun setListener(listener: IBTCallback?) {
        this.listener = listener
    }

    fun getListener(): IBTCallback? {
        return listener
    }

    fun getBtDeviceList(): MutableList<BluetoothDevice?>? {
        return ArrayList(mDeviceList?.values!!)
    }

    fun getBleConnection(address: String?): BluetoothGatt? {
        return mBluetoothGattHashMap?.get(address)
    }

    fun getDevicesNames(): ArrayList<String?>? {
        val deviceNames = ArrayList<String?>()
        for (value in mDeviceList?.values!!) {
            deviceNames.add(value?.getName())
        }
        return deviceNames
    }

    fun getBtDeviceByName(s: String?): BluetoothDevice? {
        for (value in mDeviceList?.values!!) {
            if (s == value?.getName()) return value
        }
        return null
    }

    inner class LocalBinder : Binder() {
        fun getService(): BLEService? {
            return this@BLEService
        }
    }

    companion object {
        private val TAG: String? = BLEService::class.java.simpleName
        var mBluetoothGattHashMap: HashMap<String?, BluetoothGatt?>? = HashMap()
    }
}