package efcom.avi.exercises.exercises

import android.util.Log
import efcom.avi.exercises.exercises.algorithm.SetCalculationAlgorithm
import efcom.avi.exercises.exercises.interfaces.IDeviceListener
import efcom.avi.exercises.exercises.interfaces.IExerciseManagerListener
import efcom.avi.exercises.exercises.interfaces.ISetCalculationAlgorithmListener
import efcom.avi.exercises.exercises.models.CaloriesCount
import efcom.avi.exercises.exercises.models.HFDevice
import efcom.avi.exercises.exercises.models.SetInfo
import efcom.avi.exercises.exercises.models.Weight
import java.util.*

class ExerciseManager private constructor() {
    var exerciseList: MutableList<SetInfo?>? = ArrayList()
    var map: MutableMap<HFDevice, SetCalculationAlgorithm>? = null
    private var sessionEnd = false
    var listener: IExerciseManagerListener? = null
    private val mapSetsCount: MutableMap<String?, Int?>? = HashMap()
    private val iSetCalculationAlgorithmListener: ISetCalculationAlgorithmListener? = object : ISetCalculationAlgorithmListener {
        override fun onSetStart(deviceName: String?) {
            Log.v(TAG, "onSetStart: ")
            var setsCount: Int = mapSetsCount?.get(deviceName) ?: 0
            setsCount++
            mapSetsCount?.set(deviceName, setsCount)
            listener?.onSetStart(deviceName, setsCount)
        }

        override fun onSetEnd(setInfo: SetInfo?, reason: String?) {
            exerciseList?.add(setInfo)
        }

        override fun onRepCount(name: String?, repCount: Int) {
            Log.v(TAG, "onRepCount: name: $name rep: $repCount")
            listener?.onRepCount(name, repCount)
        }
    }
    private val iDeviceListener: IDeviceListener? = object : IDeviceListener {
        override fun onDataReceived(hfDevice: HFDevice?, weights: MutableList<Weight>) {
            if (sessionEnd) return
            val setCalculationAlgorithm = map?.get(hfDevice)!!
            setCalculationAlgorithm.addData(weights)
            listener?.onCalcMaxWeight(hfDevice?.getName(), setCalculationAlgorithm.getMaxWeight())
            Log.v(TAG, "onDataReceived: active" + setCalculationAlgorithm.getSetActiveDuration() + " rest:" + setCalculationAlgorithm.getSetActiveDuration())
            listener?.onCalcCalories(CaloriesCount.CalculateCalories(setCalculationAlgorithm.getSetActiveDuration(), setCalculationAlgorithm.getSetRestDuration(), 30, 100f, 0) / DIV_CALORIES_WITH)
            for (weight in weights!!) {
                listener?.onDataReceived(hfDevice?.getName(), weight)
            }
        }

        override fun onDeviceStateChange(hfDevice: HFDevice?, state: Int) {
            if (state == 0) {
                listener?.removeDevice(hfDevice)
                map?.remove(hfDevice)
            }
        }

        override fun onBatteryLevelReceived(hfDevice: HFDevice?, batteryLevel: Int) {
            Log.v(TAG, "onBatteryLevelReceived: hfDevice: " + hfDevice?.getName() + " battery level: " + batteryLevel + "%")
        }

        override fun onVersionReceived(hfDevice: HFDevice?, version: String?) {
            Log.v(TAG, "onVersionReceived: hfDevice: " + hfDevice?.getName() + "  version: " + version)
        }

        override fun onCalibrationDone(hfDevice: HFDevice?) {
            Log.v(TAG, "onCalibrationDone: hfDevice: " + hfDevice?.getName())
        }
    }

    fun getAlgos(): MutableCollection<SetCalculationAlgorithm> {
        return map!!.values
    }

    fun initDevices(device1: HFDevice, device2: HFDevice?) {
        map = HashMap()
        val device1Algo = SetCalculationAlgorithm(device1.getName())
        device1Algo.listener = iSetCalculationAlgorithmListener
        (map as HashMap<HFDevice, SetCalculationAlgorithm>)[device1] = device1Algo
        device1.listener=(iDeviceListener)
        mapSetsCount?.set(device1.getName(), 0)
        if (device2 != null) {
            device2.listener=(iDeviceListener)
            val device2Algo = SetCalculationAlgorithm(device2.getName())
            device2Algo.listener = iSetCalculationAlgorithmListener
            (map as HashMap<HFDevice, SetCalculationAlgorithm>)[device2] = device2Algo
            mapSetsCount?.set(device2.getName(), 0)
        }
    }


    fun startSession() {
        sessionEnd = false
        for (value in map!!.values) {
            value.startSet()
        }
    }

    fun endSession() {
        sessionEnd = true
        for (value in map!!.values) {
            value.endSet()
        }
    }

    fun pauseSession() {
        for (value in map!!.values) {
            value.pauseSet()
        }
    }

    fun resumeSession() {
        for (value in map!!.values) {
            value.resumeSet()
        }
    }

    fun getTotalDuration(): Int {
        var duration = 0
        for (setInfo: SetInfo? in exerciseList!!) {
            duration += setInfo?.duration!!
        }
        return duration
    }

    fun getTotalRestDuration(): Int {
        var restDuration = 0
        for (setInfo in exerciseList!!) {
            restDuration += setInfo?.restDuration!!
        }
        return restDuration
    }

    fun getTotalMaxPower(): Int {
        var maxPower = 0
        for (setInfo in exerciseList!!) {
            maxPower += setInfo?.maxPower?.toInt()!!
        }
        return maxPower
    }

    fun getTotalMaxWeight(): Int {
        var maxWeight = 0
        for (setInfo in exerciseList!!) {
            maxWeight += setInfo?.maxWeight!!
        }
        return maxWeight
    }

    fun getTotalRepetition(): Int {
        var repetition = 0
        for (setInfo in exerciseList!!) {
            repetition += setInfo!!.repetition!!
        }
        return repetition
    }

    fun getSetsCount(): Int {
        return exerciseList?.size!!
    }

    fun getTotalWork(): Int {
        var work = 0
        for (setInfo in exerciseList!!) {
            work += setInfo?.work?.toInt()!!
        }
        return work
    }

    companion object {
        private val TAG: String? = ExerciseManager::class.java.simpleName
        const val DIV_CALORIES_WITH = 1000
        private var instance: ExerciseManager? = null
        //singleton
        fun getInstance(): ExerciseManager? {
            if (instance == null) {
                instance = ExerciseManager()
            }
            return instance
        }
    }
}