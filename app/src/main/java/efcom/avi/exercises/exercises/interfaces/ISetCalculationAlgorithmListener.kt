package efcom.avi.exercises.exercises.interfaces

import efcom.avi.exercises.exercises.models.SetInfo

interface ISetCalculationAlgorithmListener {
    open fun onSetStart(name: String?)
    open fun onSetEnd(setInfo: SetInfo?, reason: String?) // setInfo hold full list of wieghts + statistics
    open fun onRepCount(name: String?, repCount: Int)
}