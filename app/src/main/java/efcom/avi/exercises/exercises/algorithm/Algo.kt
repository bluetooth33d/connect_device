package efcom.avi.exercises.exercises.algorithm

import efcom.avi.exercises.exercises.models.Weight
import java.util.*
import kotlin.collections.ArrayList

class Algo {
    private var data: ArrayList<Double> = ArrayList<Double>();
    var minMaxMinList: ArrayList<RepInfo> = ArrayList()


    fun AppendPoints(pts: MutableList<Weight>): Int {
        if (data == null) data = ArrayList()
        for (pt in pts) {
            data.add(pt.weight.toDouble())
        }
        return RunRepsCount()
    }

    enum class AlgoState {
        Idle, SeekMax, SeekMin
    }

    val dataRate = 25.0 // 25ms 40fps
    val FPS = 1000 / dataRate
    var MinInRepTrh = 500.0
    var secondsToLook = 6.0
    var MinSecondsToLook = 1.0
    var MAX_FWHH = 150.0
    var MIN_FWHH = 10.0
    val PS = 10
    var idx = 0
    //int lastIdx = 0;
    var reps = 0
    var state: AlgoState = AlgoState.Idle
    // geberal concept - Find min, find max, find min - assume full rep. calc HHFW
    var mmm: RepInfo = RepInfo()

    fun getMaxWeight(): Double {
        return if (minMaxMinList.size < 1) {
            0.0
        } else minMaxMinList.get(minMaxMinList.size - 1).maxW
    }

    fun RunRepsCount(): Int { // check if enough tail points:
        if (data.size - idx < MinSecondsToLook / 2 * FPS) return reps
        while (true) {
            if (idx >= data.size) break
            when (state) {
                AlgoState.Idle -> {
                    if (data.get(idx) >= MinInRepTrh) // search for rep start
                    {
                        mmm = RepInfo()
                        mmm.min1 = idx
                        state = AlgoState.SeekMax
                    }
                    idx++
                }
                AlgoState.SeekMax -> {
                    val maxIdx = FindMax(idx)
                    if (maxIdx > 0) { //if (maxIdx - lastIdx > )
// check is valid
                        idx = maxIdx
                        mmm.max = idx
                        state = AlgoState.SeekMin
                    } else if (data.size - idx > FPS * secondsToLook) // check tail long enoguh to switch state
                    {
                        idx++
                        state = AlgoState.Idle
                        return reps
                    }
                    return reps
                }
                AlgoState.SeekMin -> {
                    val minIdx = FindMin(idx)
                    if (minIdx > 0) {
                        if (minIdx - idx < MIN_FWHH) { // not real index
                            idx = minIdx
                            return reps
                        }
                        //if (maxIdx - lastIdx > )
// check is valid
                        idx = minIdx
                        state = AlgoState.SeekMax
                        mmm.min2 = idx
                        val FWHH = CalcFWHH(mmm)
                        if (FWHH > MIN_FWHH && FWHH < MAX_FWHH && mmm.DeltaH > 500) // TBD
                        {
                            reps++
                            mmm.rep = reps
                            minMaxMinList.add(mmm)
                            //                            NewRep ?.Invoke(reps, FWHH, mmm);
                        }
                        if (reps == 9) {
                            val a = 0
                        }
                        mmm = RepInfo()
                        mmm.min1 = idx
                    } else if (data.size - idx > FPS * secondsToLook) // check tail long enoguh to switch state
                    {
                        idx++
                        state = AlgoState.Idle
                        return reps
                    }
                    return reps
                }
            }
        }
        return reps
    }

    private fun CalcFWHH(mmm: RepInfo): Double {
        mmm.min1W = data.get(mmm.min1)
        mmm.min2W = data.get(mmm.min2)
        mmm.maxW = data.get(mmm.max)
        val avgMin = (mmm.min2W + mmm.min1W) / 2.0
        val hh = (mmm.maxW - avgMin) / 2 + avgMin
        mmm.DeltaH = mmm.maxW - avgMin
        // find indx near hh?
        var idx1 = 0
        var idx2 = 0
        for (i in mmm.min1 until mmm.min2) {
            if (idx1 == 0 && data.get(i) > hh) idx1 = i
            if (idx1 > 0 && idx2 == 0 && data.get(i) < hh) idx2 = i
        }
        mmm.FWHH = idx2 - idx1.toDouble()
        return (idx2 - idx1).toDouble() // now in indexes. should be seconds!
    }

    private fun FindMax(idx: Int): Int {
        var maxIdx = -1
        var max = 0.0
        // find a point that has at least MaxDeltaTrh drop around
        var i = idx + PS + 1
        while (i < Math.min(data?.size?.toDouble()!!, idx + FPS * secondsToLook) - PS - 1) {
            // look for max from all points that satispy this condiion!
            var dataOut: DataOut? = DataOut()
            var d0: Double
            var d1: Double
            dataOut = GetAvg(i, PS)
            //if (d0 > 0 && d1 < 0)
//   return i;
            if (dataOut?.d0!! < data?.get(i)!! && dataOut.d1 < data?.get(i)!!) {
                if (data?.get(i)!! > max) {
                    maxIdx = i
                    max = data?.get(i)!!
                }
            }
            i++
        }
        return maxIdx
    }

    private fun FindMin(idx: Int): Int {
        var minIdx = -1
        var min = Double.MAX_VALUE
        // find a point that has at least MaxDeltaTrh drop around
        var i = idx + PS
        while (i < Math.min(data?.size?.toDouble()!!, idx + dataRate * secondsToLook) - PS) {
            var d0: Double
            var d1: Double
            var dataOut: DataOut? = DataOut()
            dataOut = GetAvg(i, PS)
            //if (d0 < 0 && d1 >= 0) // right side can be 0
//    return i;
            if (dataOut?.d0!! > data?.get(i)!! && dataOut.d1 >= data?.get(i)!!) {
                if (data?.get(i)!! < min) {
                    minIdx = i
                    min =data?.get(i)!!
                }
            }
            i++
        }
        return minIdx
    }

    private fun GetAvg(center: Int, ps: Int): DataOut? {
        var d0 = 0.0
        var d1 = 0.0
        for (i in center - ps until center) d0 += data?.get(i)!!
        for (i in center + 1..center + ps) d1 += data?.get(i)!!
        d0 = d0 / ps
        d1 = d1 / ps
        return DataOut(d0, d1)
    }

    private fun GetDAvg(center: Int, ps: Int): DataOut? {
        var d0 = 0.0
        var d1 = 0.0
        for (i in center - ps..center) d0 += data?.get(i)!! - data?.get(i - 1)!!
        for (i in center..center + ps) d1 += data?.get(i)!! - data?.get(i - 1)!!
        d0 = d0 / ps
        d1 = d1 / ps
        return DataOut(d0, d1)
    }

    //    public void SaveStatisticsFile(string csvPath) {
//        string s = csvPath.Replace(".csv", "_STATISTICS.csv");
//
//        StringBuilder sb = new StringBuilder();
//        sb.AppendLine("Rep,startIdx,maxIdx,endIdx,Amp,HW");
//
//        foreach(var mmm in minMaxMinList)
//        {
//            sb.AppendLine($"{mmm.rep},{mmm.min1},{mmm.max},{mmm.min2},{mmm.maxW-(mmm.min1W + mmm.min2W)/2},{mmm.FWHH}");
//        }
//
//        File.WriteAllText(s, sb.ToString());
//    }
    private inner class DataOut {
        constructor(d0: Double, d1: Double) {
            this.d0 = d0
            this.d1 = d1
        }

        var d0 = 0.0
        var d1 = 0.0

        constructor() {}
    }
}