package efcom.avi.exercises.exercises.models

import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

class Weight(@field:SerializedName("timeStamp") @field:Expose var timeStamp: Long, @field:SerializedName("weight") @field:Expose var weight: Int) {

    override fun toString(): String {
        return weight.toString()
    }

    fun toJson(): String? {
        val gson = Gson()
        val type = object : TypeToken<Weight?>() {}.type
        return gson.toJson(this, type)
    }

    companion object {
        fun fromJson(json: String?): Weight? {
            val gson = Gson()
            val type = object : TypeToken<Weight?>() {}.type
            return gson.fromJson(json, type)
        }
    }

}