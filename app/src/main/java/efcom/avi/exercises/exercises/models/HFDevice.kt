package efcom.avi.exercises.exercises.models

import android.bluetooth.*
import android.content.Context
import android.util.Log
import efcom.avi.exercises.exercises.Constants
import efcom.avi.exercises.exercises.interfaces.IDeviceListener
import efcom.avi.exercises.exercises.models.HFDevice
import java.util.*
import kotlin.experimental.and

class HFDevice(var device: BluetoothDevice?, context: Context?) {
    private var bluetoothGatt: BluetoothGatt?
    var listener: IDeviceListener? = null
    private var attemptConnect = 5
    var count = 0
    private var packetLastTime: Long = 0
    private var dataCount = 0
    //todo remove mock
    fun reqBatteryLevelAsync() {
        listener?.onBatteryLevelReceived(this, Random().nextInt(100))
    }

    fun reqVersionAsync() {
        listener?.onVersionReceived(this, "sw: mock hw: mock")
    }

    fun calibrateLC() {
        listener?.onCalibrationDone(this)
    }

    private val bluetoothGattCallback: BluetoothGattCallback? = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            Log.v(TAG, "onConnectionStateChange: gatt, status, newState:$gatt $status $newState")
          listener?.onDeviceStateChange(this@HFDevice, newState)
            if (newState == BluetoothGatt.STATE_CONNECTED && status == BluetoothGatt.GATT_SUCCESS) {
                gatt?.discoverServices()
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                listener?.onDeviceStateChange(this@HFDevice, newState)
                gatt?.disconnect()
                gatt?.close()
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            bluetoothGatt = gatt
            enableNotify(gatt!!)
        }

        private fun enableNotify(bluetoothGatt: BluetoothGatt) {
            val characteristic: BluetoothGattCharacteristic?
            characteristic = bluetoothGatt.getService(Constants.LS_SERVICE).getCharacteristic(Constants.FIRST_CHAR)
            bluetoothGatt.setCharacteristicNotification(characteristic, true)
            val descriptor = characteristic.getDescriptor(Constants.DESCRIPTOR_LS)
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            bluetoothGatt.writeDescriptor(descriptor)
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)
            Log.v(TAG, "onCharacteristicRead: characteristic:" + characteristic?.getUuid() + " " + Arrays.toString(characteristic?.getValue()))
            if (characteristic?.getUuid() == Constants.BATTERY_LEVEL) if (listener != null) listener?.onBatteryLevelReceived(this@HFDevice, characteristic?.getValue()!![0].toInt())
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)
            Log.v(TAG, "onCharacteristicWrite: characteristic:" + characteristic?.getUuid() + " " + Arrays.toString(characteristic?.getValue()))
            Log.v(TAG, "onCharacteristicWrite: characteristic:" + characteristic?.getUuid() + " " + String(characteristic?.getValue()!!))
            val action = String(characteristic?.getValue()!!).trim { it <= ' ' }
            if (listener != null) {
                when (action) {
                    "AT+CALI=LC" -> listener?.onCalibrationDone(this@HFDevice) //"Calibrate LC: " + gatt.getDevice().getName() + "::" + gatt.getDevice().getAddress());
                    "AT+VERSION" -> {
                    }
                }
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)
            Log.v(TAG, "onCharacteristicChanged: characteristic:" + characteristic?.getUuid() + " " + Arrays.toString(characteristic?.getValue()))
            if (listener != null) {
                val data = characteristic?.getValue()!!
                when (characteristic.getValue()[0]) {
                    (-10).toByte() -> {
                        val header = 3
                        val verSW = ByteArray(6)
                        val verHW = ByteArray(4)
                        System.arraycopy(data, header, verSW, 0, verSW.size)
                        System.arraycopy(data, header + verSW.size, verHW, 0, verHW.size)
                        listener?.onVersionReceived(this@HFDevice, "sw: " + String(verSW) + " hw: " + String(verHW))
                    }
                    (-11).toByte() -> {

                        val counter = dataCount++
                        Log.v(TAG, "onCharacteristicChanged: " + Date().time + " " + counter)
                        synchronized(this@HFDevice) {
                            Log.v(TAG, "onCharacteristicChanged: " + Date().time + " " + counter)
                            val weights: MutableList<Weight> = ArrayList()
                            val timePackageArrived = Date().time
                            var time = timePackageArrived
                            var diff = Math.max(Math.min(time - packetLastTime, 200).toInt(), 8)
                            diff -= diff % 8
                            time -= diff.toLong()
                            val delta = diff / 8
                            Log.v(TAG, "onCharacteristicChanged: time: $time packetLastTime: $packetLastTime diff: $diff")
                            if (data.size > 2 && data[0].toInt() and 0xFF == 0xf5) {
                                var i = 3
                                while (i < data.size - 1) {
                                    val weight_calc: Int = (((data[i].toUByte()) + (data[i + 1].toUByte()) * 256u) * 10u).toInt()
                                    weights.add(Weight(time + (i - 1) / 2 * delta, weight_calc))
                                    //                                weights.add(new Weight(count++,weight_calc));
                                    Log.v(TAG, "onDataReceived: parse:$weight_calc")
                                    i += 2
                                }
                            }
                            packetLastTime = timePackageArrived
                            listener?.onDataReceived(this@HFDevice, weights)
                        }
                    }
                }
            }
        }

        override fun onDescriptorRead(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?, status: Int) {
            super.onDescriptorRead(gatt, descriptor, status)
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            attemptConnect = 5
        }
    }

    fun getName(): String? {
        return if (device == null) "NONE" else device?.getName()
    }

    companion object {
        private val TAG: String? = "HFDevice"
    }

    init {
        bluetoothGatt = device?.connectGatt(context, false, bluetoothGattCallback, BluetoothDevice.TRANSPORT_LE)
    }
}