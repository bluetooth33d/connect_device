package efcom.avi.exercises.chartutils

import android.content.Context
import android.util.Log
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import efcom.avi.exercises.chartutils.LineChartCreator
import java.util.*

class LineChartCreator(private val mContext: Context?, private val mChart: LineChart?, iOnDataSetDataChanged: IOnDataSetDataChanged?) {
    private var mLineData: LineData? = null
    private val mDataSetsHashMap: HashMap<String?, LineDataSet?>?
    private var mRssiChartDataProvider: RssiChartDataProvider? = null
    private val mIOnDataSetDataChanged: IOnDataSetDataChanged?
    private fun initChart() {
        mRssiChartDataProvider = RssiChartDataProvider(mChart, mIOnDataSetDataChanged)
        mChart?.getDescription()?.isEnabled = false
        mChart?.getLegend()?.isWordWrapEnabled = true
        mChart?.getLegend()?.form = Legend.LegendForm.CIRCLE
        mChart?.getLegend()?.isEnabled = false
        val rightAxis = mChart?.getAxisRight()
        rightAxis?.setDrawGridLines(false)
        rightAxis?.axisMinimum = 0f // this replaces setStartAtZero(true)
        rightAxis?.isEnabled = false
        rightAxis?.setDrawAxisLine(false)
        val xAxis = mChart?.getXAxis()
        xAxis?.position = XAxis.XAxisPosition.BOTTOM
        xAxis?.axisMinimum = 0f
        xAxis?.granularity = 1f
        xAxis?.setDrawGridLines(false)
        xAxis?.setDrawAxisLine(true)
        xAxis?.axisLineWidth = 2.0f
        xAxis?.setAvoidFirstLastClipping(true)
        xAxis?.setCenterAxisLabels(true)
        //        xAxis.setValueFormatter(mRssiChartDataProvider);
        val leftAxis = mChart?.getAxisLeft()
        leftAxis?.setDrawGridLines(false)
        leftAxis?.axisMinimum = 0f // this replaces setStartAtZero(true)
        leftAxis?.axisLineWidth = 2.0f
        leftAxis?.setDrawAxisLine(true)
        leftAxis?.isGranularityEnabled = true
        leftAxis?.granularity = 5f
        mChart?.setMarker(CustomMarkerView(mContext))
        mLineData = LineData(ArrayList())
        mChart?.setData(mLineData)
    }

    fun addEntry(rssiEntry: RssiEntry?) {
        try {
            mRssiChartDataProvider?.addEntry(rssiEntry, true)
            mChart?.setVisibleXRangeMaximum(80f)
            val lineData = mChart?.getLineData()
            mChart?.moveViewTo(mChart.getLineData().xMax, (lineData?.yMax!! + lineData.yMin) / 2, YAxis.AxisDependency.RIGHT)
            //            mChart.moveViewToX(mChart.getLineData().getXMax());
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //        mChart.moveViewToAnimated(mChart.getLineData().getXMax(), 0, YAxis.AxisDependency.RIGHT, 1000);
    }

    fun addEntry2(rssiEntry: RssiEntry?) {
        var dataSetIndex = getDataSetIndexByLabel(rssiEntry?.getMACAddress())
        Log.v("INDX", "DATA SET INDEX = $dataSetIndex")
        if (dataSetIndex == -1) {
            dataSetIndex = createAndAddNewDataSet(rssiEntry?.getMACAddress())
        }
        val data = mChart?.getData()
        if (data != null) {
            data.addEntry(rssiEntry?.getAsEntry(), dataSetIndex)
            // let the chart know it's data has changed
            mChart?.notifyDataSetChanged()
            //            mChart.fitScreen();
        }
    }

    private fun createAndAddNewDataSet(label: String?): Int {
        val lineDataSet = mRssiChartDataProvider?.generateNewLineDataSet(label)
        mChart?.getLineData()?.addDataSet(lineDataSet)
        return getDataSetIndexByLabel(label)
    }

    private fun getDataSetIndexByLabel(label: String?): Int {
        val iLineDataSet = mChart?.getLineData()?.getDataSetByLabel(label, true) ?: return -1
        return mChart.getLineData().getIndexOfDataSet(iLineDataSet)
    }

    fun getAllDataSets(): ArrayList<LineDataSet?>? {
        val result = mRssiChartDataProvider?.getLineDataSets()
        Log.v(TAG, "RESS = " + (result?.size ?: "NULL!"))
        return result
    }

    fun makeDataSetInvisible(lineDataSet: LineDataSet?) {
        mChart?.highlightValues(null)
        mRssiChartDataProvider?.makeDataSetInvisible(lineDataSet)
    }

    fun makeDataSetVisible(lineDataSet: LineDataSet?) {
        mChart?.highlightValues(null)
        mRssiChartDataProvider?.makeDataSetVisible(lineDataSet?.getLabel())
    }

    fun isDataSetCurrentlyVisible(lineDataSet: LineDataSet?): Boolean {
        return mRssiChartDataProvider?.isDataSetCurrentlyVisible(lineDataSet)!!
    }

    companion object {
        private val TAG: String? = LineChartCreator::class.java.simpleName
    }

    init {
        mDataSetsHashMap = HashMap()
        mIOnDataSetDataChanged = iOnDataSetDataChanged
        initChart()
    }
}