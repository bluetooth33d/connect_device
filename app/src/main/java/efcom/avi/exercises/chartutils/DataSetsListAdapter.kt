package efcom.avi.exercises.chartutils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.github.mikephil.charting.data.LineDataSet
import efcom.avi.exercises.R
import java.util.*

/**
 * Created by Efcom on 16-Oct-17.
 */
class DataSetsListAdapter(c: Context?) : BaseAdapter() {
    private val mContext: Context?
    private var mList: ArrayList<LineDataSet?>?
    //    private ArrayList<MiniDataSet> mList;
    private val mLayoutInflater: LayoutInflater?
    private var mIOnDataSetVisibilityChange: IOnDataSetVisibilityChange? = null
    fun setIOnDataSetVisibilityChange(iOnDataSetVisibilityChange: IOnDataSetVisibilityChange?) {
        mIOnDataSetVisibilityChange = iOnDataSetVisibilityChange
    }

    fun updateList(list: ArrayList<LineDataSet?>?) {
        if (list == null) {
            Log.v(TAG, "list IS NULL! ")
            return
        }
        var daeda = ""
        for (i in list.indices) {
            daeda += " " + list[i]?.getLabel()
        }
        Log.v(TAG, "list = $daeda")
        try {
            mList = list
            //            mList = createListFromDataSetItems(list);
            notifyDataSetChanged()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getCount(): Int {
        return if (mList == null) 0 else mList?.size!!
    }

    override fun getItem(i: Int): Any? {
        return mList?.get(i)
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(position: Int, view: View?, viewGroup: ViewGroup?): View? {
        val row = mLayoutInflater?.inflate(R.layout.item_dataset_list_layout, null, false)
        val customCircleView = CustomCircleView(mContext)
        customCircleView.setColor(mList?.get(position)?.getColor()!!)
        val circleContainter = row?.findViewById<View?>(R.id.circleView) as RelativeLayout?
        circleContainter?.addView(customCircleView)
        val macTV = row?.findViewById<View?>(R.id.MacAddressTV) as TextView?
        Log.v(TAG, "getView: " + mList?.get(position)!!.getLabel())
        macTV?.setText(mList?.get(position)!!.getLabel())
        val toggleButton = row?.findViewById<View?>(R.id.VisabilityToggleBtn) as ToggleButton?
        toggleButton?.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (mIOnDataSetVisibilityChange == null) return@OnCheckedChangeListener
            if (b) mIOnDataSetVisibilityChange!!.makeDataSetVisible(mList!!.get(position)) else mIOnDataSetVisibilityChange!!.makeDataSetInvisible(mList!!.get(position))
        })
        if (mIOnDataSetVisibilityChange != null) toggleButton?.setChecked(mIOnDataSetVisibilityChange!!.isDataSetCurrentlyVisible(mList!!.get(position)))
        return row
    }

    inner class CustomCircleView(context: Context?) : View(context) {
        var mColor = 0
        fun setColor(color: Int) {
            mColor = color
        }

        override fun onDraw(canvas: Canvas?) {
            super.onDraw(canvas)
            val circlePaint = Paint()
            circlePaint.color = mColor
            canvas?.drawCircle(height / 2.toFloat(), width / 2.toFloat(), height / 2 - 40.toFloat(), circlePaint)
        }
    }

    companion object {
        private val TAG: String? = DataSetsListAdapter::class.java.simpleName
    }

    init {
        mList = ArrayList()
        mContext = c
        mLayoutInflater = LayoutInflater.from(c)
    }
}