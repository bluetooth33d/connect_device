package efcom.avi.exercises.chartutils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.view.View
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import efcom.avi.exercises.R

/**
 * Created by Efcom on 16-Oct-17.
 */
class CustomMarkerView(private val mContext: Context?) : MarkerView(mContext, R.layout.marker_view_layout) {
    private var mTv: TextView?
    override fun refreshContent(e: Entry?, highlight: Highlight?) {
        super.refreshContent(e, highlight)
        if (mTv == null) mTv = findViewById<View?>(R.id.TextViewContent) as TextView?
        val rssiEntry = e?.getData() as RssiEntry
        mTv?.setText(rssiEntry.getRssiVal().toString() + "")
        val shape = findViewById<View?>(R.id.MarkerBackgroundRelativeLayout)?.getBackground() as LayerDrawable
        //        LayerDrawable shape = (LayerDrawable) ContextCompat.getDrawable(mContext, R.drawable.white_rounded_background_with_shadow_drawable);
        var color = rssiEntry.getColor()
        color = Color.argb(42, Color.red(color), Color.green(color), Color.blue(color))
        val gradientDrawable = shape.findDrawableByLayerId(R.id.RoundedBackground) as GradientDrawable
        gradientDrawable.setColor(color) // changing the back color
        val view = findViewById<View?>(R.id.MarkerBackgroundRelativeLayout)
        view?.setX(-view.getWidth() / 2.toFloat())
        view?.setY(-20f - view.getHeight())
    }

    init {
        mTv = findViewById<View?>(R.id.TextViewContent) as TextView?
    }
}