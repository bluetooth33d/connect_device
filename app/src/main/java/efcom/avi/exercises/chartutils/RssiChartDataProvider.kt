package efcom.avi.exercises.chartutils

import android.util.Log
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import efcom.avi.exercises.chartutils.RssiChartDataProvider
import java.text.SimpleDateFormat
import java.util.*

class RssiChartDataProvider(private val mChart: LineChart?, private val mIOnDataSetDataChanged: IOnDataSetDataChanged?) : IAxisValueFormatter {
    private var mLineDataSets: ArrayList<LineDataSet?>? = null
    private var mVisibleSetsLabels: ArrayList<String?>? = null
    private var mXLabels: ArrayList<String?>? = null
    fun addDataSet(lineDataSet: LineDataSet?): Int {
        if (mLineDataSets == null) mLineDataSets = ArrayList()
        if (mLineDataSets?.contains(lineDataSet)!!) return mLineDataSets?.indexOf(lineDataSet)!!
        mLineDataSets?.add(lineDataSet)
        mIOnDataSetDataChanged?.onChange(mLineDataSets)
        return mLineDataSets!!.indexOf(lineDataSet)
    }

    fun getDataSetIndex(lineDataSet: LineDataSet?): Int {
        return mLineDataSets!!.indexOf(lineDataSet)
    }

    fun addEntry(rssiEntry: RssiEntry?, makeDataSetVisible: Boolean) {
        Log.v("PPS", "ADD ENTRY " + rssiEntry.toString())
        if (!isThereSuchLabeledDataSet(rssiEntry?.getMACAddress())) {
            addDataSet(generateNewLineDataSet(rssiEntry?.getMACAddress()))
            addToVisibleList(rssiEntry?.getMACAddress())
            makeVisibleOnGraph(rssiEntry?.getMACAddress())
        }
        val lineDataSet = getDataSetByLabel(rssiEntry?.getMACAddress())
        if (lineDataSet == null) {
            Log.v(TAG, "LINE DATA SET IS NULLLL!!!")
            return
        }
        addToXLabels(rssiEntry!!.getTimeStamp())
        val entry = Entry(getXLabelIndex(rssiEntry.getTimeStamp()).toFloat(), rssiEntry.getRssiVal().toFloat(), rssiEntry)
        rssiEntry.setColor(lineDataSet.color)
        lineDataSet.addEntry(entry)
        //        lineDataSet.addEntry(rssiEntry.getAsEntry());
//        if (makeDataSetVisible) {
//            addToVisibleList(rssiEntry.getMACAddress());
//            makeVisibleOnGraph(rssiEntry.getMACAddress());
//        }
        if (mVisibleSetsLabels?.contains(rssiEntry.getMACAddress())!!) {
            mChart?.getLineData()?.notifyDataChanged()
            mChart?.notifyDataSetChanged()
            mChart?.invalidate()
            //            mChart.fitScreen();
//            mChart.moveViewToX(mChart.getLineData().getXMax());
        }
    }

    private fun getXLabelIndex(timeStamp: Long): Int {
        var res: String
        val dateFormat = SimpleDateFormat("mm:ss")
        res = dateFormat.format(Date(timeStamp))
        res = timeStamp.toString()
        val result = getXLabelIndex(res)
        Log.v("TA", "" + result)
        return result
    }

    private fun addToXLabels(timeStamp: Long) {
        var res: String
        val dateFormat = SimpleDateFormat("mm:ss")
        res = dateFormat.format(timeStamp)
        res = timeStamp.toString()
        addToXLabels(res)
    }

    fun makeDataSetVisible(label: String?) {
        addToVisibleList(label)
        makeVisibleOnGraph(label)
    }

    private fun makeVisibleOnGraph(label: String?) {
        if (mChart == null) return
        val lineData = mChart.lineData
        val lineDataSet = lineData.getDataSetByLabel(label, true)// as LineDataSet
        if (lineDataSet != null) // Data Set Already Included..
            return
        lineData.addDataSet(getDataSetByLabel(label))
    }

    private fun addToVisibleList(label: String?) {
        if (mVisibleSetsLabels == null) mVisibleSetsLabels = ArrayList()
        if (!mVisibleSetsLabels!!.contains(label)) mVisibleSetsLabels!!.add(label)
    }

    private fun isThereSuchLabeledDataSet(label: String?): Boolean {
        if (mLineDataSets == null) {
            mLineDataSets = ArrayList()
            return false
        }
        for (i in mLineDataSets!!.indices) {
            if (mLineDataSets!!.get(i)!!.getLabel() == label) return true
        }
        return false
    }

    private fun getDataSetByLabel(label: String?): LineDataSet? {
        for (i in mLineDataSets!!.indices) {
            if (mLineDataSets!!.get(i)?.getLabel() == label) return mLineDataSets!!.get(i)
        }
        return null
    }

    fun generateNewLineDataSet(label: String?): LineDataSet? {
        val result = LineDataSet(ArrayList(), label)
        val color = Utils.generateColor()
        result.color = color
        result.setCircleColor(color)
        result.setCircleColorHole(color)
        result.setDrawValues(false)
        result.lineWidth = 1.2f
        result.highLightColor = color
        return result
    }

    fun addToXLabels(lbl: String?) {
        if (mXLabels == null) mXLabels = ArrayList()
        if (!mXLabels!!.contains(lbl)) mXLabels!!.add(lbl)
    }

    fun getXLabelIndex(lbl: String?): Int {
        if (mXLabels == null) {
            mXLabels = ArrayList()
            return -1
        }
        return if (!mXLabels!!.contains(lbl)) -1 else mXLabels!!.indexOf(lbl)
    }

    fun getLineDataSets(): ArrayList<LineDataSet?>? {
        return mLineDataSets
    }

    override fun getFormattedValue(value: Float, axis: AxisBase?): String? {
        if (mXLabels == null) mXLabels = ArrayList()
        return if (mXLabels!!.size <= value || value < 0) "" else mXLabels!!.get(value.toInt())
    }

    fun makeDataSetInvisible(lineDataSet: LineDataSet?) {
        if (!mVisibleSetsLabels!!.contains(lineDataSet?.getLabel())) return  //   Already Invisble!
        mVisibleSetsLabels!!.remove(lineDataSet?.getLabel())
        mChart?.getLineData()?.removeDataSet(lineDataSet)
    }

    fun isDataSetCurrentlyVisible(lineDataSet: LineDataSet?): Boolean {
        return mVisibleSetsLabels?.contains(lineDataSet?.getLabel()) ?: false
    }

    companion object {
        private val TAG: String? = RssiChartDataProvider::class.java.simpleName
    }

}