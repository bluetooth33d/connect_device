package efcom.avi.exercises.chartutils

import com.github.mikephil.charting.data.LineDataSet
import java.util.*

/**
 * Created by Efcom on 16-Oct-17.
 */
interface IOnDataSetDataChanged {
    open fun onChange(updatedSets: ArrayList<LineDataSet?>?)
}