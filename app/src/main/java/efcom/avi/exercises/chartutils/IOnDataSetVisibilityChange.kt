package efcom.avi.exercises.chartutils

import com.github.mikephil.charting.data.LineDataSet

/**
 * Created by Efcom on 16-Oct-17.
 */
interface IOnDataSetVisibilityChange {
    open fun makeDataSetVisible(lineDataSet: LineDataSet?)
    open fun makeDataSetInvisible(lineDataSet: LineDataSet?)
    open fun isDataSetCurrentlyVisible(lineDataSet: LineDataSet?): Boolean
}