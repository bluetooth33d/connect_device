package efcom.avi.exercises.chartutils

import com.github.mikephil.charting.data.Entry
import java.util.*

/**
 * Created by Efcom on 15-Oct-17.
 */
class RssiEntry(private val mTimeStamp: Long, private val mMACAddress: String?, private val mRssiVal: Int) {
    private var mColor = 0
    fun getTimeStamp(): Long {
        return mTimeStamp
    }

    fun getRssiVal(): Int {
        return mRssiVal
    }

    fun getMACAddress(): String? {
        return mMACAddress
    }

    fun getAsEntry(): Entry? {
        val cal = Calendar.getInstance()
        cal.timeInMillis = mTimeStamp
        cal[Calendar.HOUR_OF_DAY] = 0
        cal[Calendar.MINUTE] = 0
        cal[Calendar.SECOND] = 0
        cal[Calendar.MILLISECOND] = 0
        //        return new Entry(mTimeStamp - cal.getTimeInMillis(), mRssiVal, this);
        return Entry((mTimeStamp - cal.timeInMillis).toFloat(), mRssiVal.toFloat(), this)
    }

    override fun toString(): String {
        return "RssiEntry{" +
                "mTimeStamp=" + mTimeStamp +
                ", mMACAddress='" + mMACAddress + '\'' +
                ", mRssiVal=" + mRssiVal +
                '}'
    }

    fun setColor(color: Int) {
        mColor = color
    }

    fun getColor(): Int {
        return mColor
    }

    companion object {
        private val TAG: String? = RssiEntry::class.java.simpleName
    }

}