package efcom.avi.exercises.chartutils

import android.graphics.Color
import java.util.*

/**
 * Created by Efcom on 15-Oct-17.
 */
object Utils {
    fun generateColor(): Int { // generate the random integers for r, g and b value
        val rand = Random()
        val r = rand.nextInt(255)
        val g = rand.nextInt(255)
        val b = rand.nextInt(255)
        return Color.rgb(r, g, b)
    }
}